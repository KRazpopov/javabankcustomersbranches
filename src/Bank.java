import java.util.ArrayList;

public class Bank {
        private String name;
        private ArrayList<Branch> branches;


        public Bank(String name){
            this.name = name;
            this.branches = new ArrayList<Branch>();
        }

        public boolean addBranch(String BranchName){

            if(FindBranch(BranchName)==null){
                this.branches.add(new Branch(BranchName));
                return true;
            }
            else{
                return false;
            }
        }

        public boolean addCustomer(String branchName, String name, double initialAmount){
            Branch branch = FindBranch(branchName);
            if(branch!=null){
                return branch.newCustomer(name,initialAmount);
            }
            return false;
        }
        public boolean addcustomerTransaction(String BranchName,String customerName,double amount){
            Branch branch = FindBranch(BranchName);///Checking firstly whether branch exist
            if(branch!=null){
                return branch.addCustomerTransaction(customerName,amount);
            }
            return false;
        }

    private Branch FindBranch (String Branchname){///private because it will be in usage only in his class, it is in vain to be accessible from another classes
        for(int i=0;i<this.branches.size();i++){
            Branch checkedBranch = this.branches.get(i);
            if(checkedBranch.getName().equals(Branchname)){
                return checkedBranch;
            }
        }
        return null;
    }
    ///Show list of customers per branch, and optionally list of their transactions.
    public boolean listCustomers(String BranchName,boolean ShowTransactions){
        Branch branch = FindBranch(BranchName);
        if(branch!=null){
            System.out.println("Customer details for Branch "+branch.getName());

            ArrayList<Customer> BranchCustomers = branch.getCustomers();
            for(int i=0;i<BranchCustomers.size();i++){
                Customer branchCustomer = BranchCustomers.get(i);
                System.out.println("Customers: "+ branchCustomer.getName()+"["+i+"]");
                if(ShowTransactions){
                    ArrayList<Double> transactions  = branchCustomer.getTransactions();
                    for(int j=0;j<transactions.size();j++){
                        System.out.println("["+(j+1)+"]"+transactions.get(j));
                    }

                }

            }
             return true;
        }
        else {
            return false;
        }
    }


}
